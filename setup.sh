#!/bin/ash

## Exit when any command fails
set -e

## Check if running with root permissions
if [[ $(id -u) -gt 0 ]]; then
    echo "This script must be run as root"
    exit 1
fi

## Set defaults version
version="0.2.0-beta.4"

## Get args
while getopts a:o:v opt; do
    case $opt in
    a) arch="$OPTARG" ;;
    o) os="$OPTARG" ;;
    v) version="$OPTARG" ;;
    esac
done

## Check args
if [ -z "$arch" ]; then
    echo "arch is unset $arch"
    exit 1
fi
if [ -z "$os" ]; then
    echo "os is unset $os"
    exit 1
fi
if [ -z "$version" ]; then
    echo "version is unset $version"
    exit 1
fi

## Download Wiretrustee
url="https://github.com/wiretrustee/wiretrustee/releases/download/v${version}/wiretrustee_${version}_${os}_${arch}.tar.gz"
echo "Downloading $url"
curl "$url" -sLo /tmp/wiretrustee.tar.gz

## Extract Wiretrustee
echo "Install binary"
mkdir /tmp/wiretrustee
tar xzf /tmp/wiretrustee.tar.gz -C /tmp/wiretrustee
mv /tmp/wiretrustee/wiretrustee /usr/bin/wiretrustee
rm -fr /tmp/wiretrustee /tmp/wiretrustee.tar.gz

## Create config directory
mkdir -p /etc/wiretrustee

## Download service file
echo "Add service file"
service_file="/etc/init.d/wiretrustee"
if [ ! -f "$service_file" ]; then
    curl https://gitlab.com/walnuss0815/wiretrustee-openwrt/-/raw/main/files/etc/init.d/wiretrustee -sLo "$service_file"
fi

## Set permissions
chmod 755 "$service_file"
chown root:root "$service_file"

## Download config file
echo "Add service config file"
config_file="/etc/config/wiretrustee"
if [ ! -f "$config_file" ]; then
    curl https://gitlab.com/walnuss0815/wiretrustee-openwrt/-/raw/main/files/etc/config/wiretrustee -sLo "$config_file"
fi

echo "Installation succeed!"
