# Wiretrustee OpenWRT

This script installs Wiretrustee on OpenWRT devices.

## Usage

<pre>
./setup.sh -a mips_softfloat -o linux -v 0.2.0-beta.4
wiretrustee login --setup-key ENTER_YOUR_KEY
</pre>